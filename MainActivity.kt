package com.example.mhairat.muth


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import net.objecthunter.exp4j.ExpressionBuilder

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        textOne.setOnClickListener { appendOnExpresstion("1", true) }
        textTwo.setOnClickListener { appendOnExpresstion("2", true) }
        textThree.setOnClickListener { appendOnExpresstion("3", true) }
        textFour.setOnClickListener { appendOnExpresstion("4", true) }
        textFive.setOnClickListener { appendOnExpresstion("5", true) }
        textSix.setOnClickListener { appendOnExpresstion("6", true) }
        textSeven.setOnClickListener { appendOnExpresstion("7", true) }
        textEight.setOnClickListener { appendOnExpresstion("8", true) }
        textNine.setOnClickListener { appendOnExpresstion("9", true) }
        textZero.setOnClickListener { appendOnExpresstion("0", true) }
        textDot.setOnClickListener { appendOnExpresstion(".", true) }


        textPlus.setOnClickListener { appendOnExpresstion("+", false) }
        textMinus.setOnClickListener { appendOnExpresstion("-", false) }
        textMul.setOnClickListener { appendOnExpresstion("*", false) }
        textDivide.setOnClickListener { appendOnExpresstion("/", false) }
        textOpen.setOnClickListener { appendOnExpresstion("(", false) }
        textClose.setOnClickListener { appendOnExpresstion(")", false) }

        textClear.setOnClickListener {
            textExp.text = ""
            textRes.text = ""
        }

        textBack.setOnClickListener {
            val string = textExp.text.toString()
            if(string.isNotEmpty()){
                textExp.text = string.substring(0,string.length-1)
            }
            textRes.text = ""
        }

        textEquals.setOnClickListener {
            try {

                val expression = ExpressionBuilder(textExp.text.toString()).build()
                val result = expression.evaluate()
                val longResult = result.toLong()
                if(result == longResult.toDouble())
                    textRes.text = longResult.toString()
                else
                    textRes.text = result.toString()

            }catch (e:Exception){
                Log.d("Exception"," message : " + e.message )
            }
        }

    }

    fun appendOnExpresstion(string: String, canClear: Boolean) {

        if(textRes.text.isNotEmpty()){
            textExp.text = ""
        }

        if (canClear) {
            textRes.text = ""
            textExp.append(string)
        } else {
            textExp.append(textRes.text)
            textExp.append(string)
            textRes.text = ""
        }
    }
}
